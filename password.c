#include <stdio.h>
#include <string.h>

/*
 * FROM https://www.redhat.com/en/blog/fighting-exploits-control-flow-integrity-cfi-clang
 */

#define AUTHMAX 2048

struct auth {
 char pass[AUTHMAX];
 void (*func)(struct auth*);
};

void success() {
 printf("Authenticated successfully\n");
}

void failure() {
 printf("Authentication failed\n");
}

void auth(struct auth *a) {
 if (strcmp(a->pass, "pass") == 0)
     a->func = &success;
 else
     a->func = &failure;

}

void check_password() {
 char buf[AUTHMAX];
 struct auth a;

 a.func = &auth;

 printf("Enter your password:\n");
 scanf("%s", (char*) &a.pass);
 strncpy(buf, a.pass, AUTHMAX);

 a.func(&a);
}

int main(int argc, char **argv) {
 check_password();
 return 0;
}

