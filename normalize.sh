#!/bin/bash

create_asm() {
  for f in password_nopie password_nocfi password_nocanary password_fcf password_ind password_force_ind password_cfi; do 
    if [[ ! -f "$f" ]]; then
      echo "  MISSING $f"
      continue
    fi
    objdump --no-show-raw-insn -d -Mintel $f | cut -f2,3> ${f}.asm
    sed -i 's/\(0x[0-9a-f]\{4\}\)[0-9a-f]\{2\}/\1xx/g' "${f}.asm"
    sed -i 's/\(0x[0-9a-f]\{2\}\)[0-9a-f]\{2\}/\1xx/g' "${f}.asm"
    sed -i 's/\(00[0-9a-f]\{4\}\)[0-9a-f]\{2\}/\1xx/g' "${f}.asm"
    sed -i 's/ \([0-9a-f]\{4\}\)[0-9a-f]\{2\} / \1xx /g' "${f}.asm"
  done
}

create_asm
