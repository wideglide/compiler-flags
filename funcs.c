int sum(int x, int y) {
	  return x + y;
}

int dbl(int x) {
	  return x + x;
}

void call_fn(int (*fn)(int)) {
	  (*fn)(42);
}

void erase_type(void *fn) {
	  call_fn(fn);
}
