
CC = gcc-9

BINS = password_nopie password_nocfi password_nocanary \
       password_fcf password_ind password_force_ind \
       password_clash password_nop password_a64 password_stk_offset \
       password_cfi password_ss 

all: $(BINS)

password_ind : CC = gcc-9
password_force_ind : CC = gcc-9

main-clang: CC = clang-10
password_cfi : CC = clang-10
password_ss : CC = clang-10

main-clang: main.c funcs.c
	$(CC) -flto -fsanitize=cfi -fvisibility=default -o $@ $^

password_nopie: password.c
	$(CC) -no-pie -o $@ $<

password_nocfi: password.c
	$(CC) -no-pie  -fcf-protection=none -o $@ $<

password_nocanary: password.c
	$(CC) -no-pie  -fcf-protection=none -fno-stack-protector -o $@ $<

password_fcf: password.c
	$(CC) -no-pie -fcf-protection=full -o $@ $<

password_ind: password.c
	$(CC) -no-pie -mindirect-branch=thunk -fcf-protection=none -o $@ $<

password_force_ind: password.c
	$(CC) -no-pie -mforce-indirect-call -o $@ $<

password_clash: password.c
	$(CC) -no-pie -fstack-clash-protection -fstack-protector-strong -o $@ $<

password_nop: password.c
	$(CC) -no-pie -fpatchable-function-entry=16,8 -o $@ $<

password_a64: password.c
	$(CC) -no-pie -falign-functions=64 -o $@ $<

password_stk_offset: password.c
	$(CC) -no-pie -mstack-protector-guard-offset=64 -o $@ $<


password_cfi: password.c
	$(CC) -flto -fsanitize=cfi -fvisibility=default -o $@ $<

password_ss: password.c
	$(CC) -fsanitize=safe-stack -o $@ $<

clean: 
	rm -f main-clang password_*
